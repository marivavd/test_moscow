// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:test_sec_1_moscow/domain/get_onboarding_items.dart';
import 'package:test_sec_1_moscow/domain/queue.dart';

import 'package:test_sec_1_moscow/main.dart';
import 'package:test_sec_1_moscow/presentation/pages/onboarding.dart';

void main() {
  Queue queue = Queue();
  group("Test OnBoarding", () {
    test('−	Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь)', ()
    {
    queue.reset_queue();
    var etalon = get_onboarding_items();
    for (var val in etalon){
      var currentElement = queue.next();
      expect(currentElement, val);
    }
  });
    test('−	Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', ()
    {
      queue.reset_queue();
      int len_queue = queue.len();
      var etalon = get_onboarding_items();
      for (var val in etalon){
        queue.next();
        expect(queue.len(), len_queue - 1);
        len_queue = queue.len();
      }
    });
    testWidgets("−	В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.", (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);
        await widgetTester.tap(find.widgetWithText(FilledButton, "Next"));
        await widgetTester.pumpAndSettle();

      }
    });
    testWidgets("−	Случай, когда очередь пустая, надпись на кнопке должна измениться на Sing Up.", (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        await widgetTester.tap(find.widgetWithText(FilledButton, "Next"));
        await widgetTester.pumpAndSettle();
      }
      expect(find.widgetWithText(FilledButton, "Sign Up"), findsOneWidget);
    });
    testWidgets("−	Если очередь пустая и пользователь нажал на кнопку “Sing in”, происходит открытие пустого экрана «Holder» приложения. Если очередь не пустая – переход отсутствует.", (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(MaterialApp(
        home: OnBoarding(),
      )));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        expect(find.widgetWithText(InkWell, "Sign in"), findsNothing);
        await widgetTester.tap(find.widgetWithText(FilledButton, "Next"));
        await widgetTester.pumpAndSettle();
      }
      expect(find.widgetWithText(InkWell, "Sign in"), findsOneWidget);
      await widgetTester.tap(find.widgetWithText(InkWell, "Sign in"));
      await widgetTester.pumpAndSettle();
      expect(find.byKey(Key('Holder')), findsOneWidget); // Правильно?
    });
  });
}
