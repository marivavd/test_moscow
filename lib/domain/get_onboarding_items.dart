List<Map<String, String>> get_onboarding_items(){
  return [{
    'title': 'Quick Delivery At Your\nDoorstep',
    "text": 'Enjoy quick pick-up and delivery to\nyour destination',
    'image': 'assets/In no time-pana 1.png'
  },
    {
      'title': 'Flexible Payment',
      "text": 'Different modes of payment either\nbefore and after delivery without\nstress',
      'image': 'assets/rafiki.png'
    },
    {
      'title': 'Real-time Tracking',
      "text": 'Track your packages/items from the\ncomfort of your home till final destination',
      'image': 'assets/rafiki2.png'
    },
  ];
}
