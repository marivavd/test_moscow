

import 'package:test_sec_1_moscow/domain/get_onboarding_items.dart';

class Queue{
  List<Map<String, String>> sp = [];
  void push(Map<String, String> element){
    sp.insert(sp.length, element);

  }

  bool isEmpty(){
    return sp.isEmpty;
  }

  int len(){
    return sp.length;
  }

  Map<String, String> next(){
    var elem = sp[0];
    sp.remove(sp[0]);
    return elem;

  }

  void reset_queue(){
    sp.clear();
    sp = get_onboarding_items();
  }

}