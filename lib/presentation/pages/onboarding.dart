import 'package:flutter/material.dart';
import 'package:test_sec_1_moscow/domain/queue.dart';
import 'package:test_sec_1_moscow/presentation/pages/holder.dart';

class OnBoarding extends StatefulWidget {
  OnBoarding({super.key});

  Queue queue = Queue();

  @override
  State<OnBoarding> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OnBoarding> {
  PageController controller = PageController();
  Map<String, String> currentElement = {};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.queue.reset_queue();
    currentElement = widget.queue.next();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: PageView.builder(
          allowImplicitScrolling: false,
            physics: NeverScrollableScrollPhysics(),
            controller: controller,
            onPageChanged: (index){
              setState(() {
                currentElement = widget.queue.next();
              });
            },
            itemBuilder: (_, index){
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 111,),
                  SizedBox(
                    width: double.infinity,
                    child: Image.asset(currentElement['image']!),
                  ),
                  Spacer(),
                  Text(
                      currentElement['title']!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 24,
                          color: Color(0xFF0560FA),
                          fontWeight: FontWeight.w700
                      ),
                    ),
                  SizedBox(height: 5,),
                  Text(
                      currentElement['text']!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF3A3A3A),
                        fontWeight: FontWeight.w400,
                        height: 40/32
                    ),
                  ),
                  SizedBox(height: 82,),
                  (widget.queue.isEmpty())?
                      Padding(padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 46,
                            width: 342,
                            child: FilledButton(
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                              },
                              child: Text(
                                "Sign Up",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                ),
                              ),
                              style: ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                    side: BorderSide(width: 0, color: Color(0xFF0560FA))
                                  )
                                )
                              ),
                            ),
                          ),
                          SizedBox(height: 20,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Already have an account?',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xFFA7A7A7),
                                  fontWeight: FontWeight.w400,
                                  height: 16/14
                                ),
                              ),
                              InkWell(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                                },
                                child: Text(
                                  'Sign in',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Color(0xFF0560FA),
                                      fontWeight: FontWeight.w500,
                                      height: 16/14
                                  ),
                                ),
                              )
                            ],
                          ),

                        ],
                      ),):
                      Padding(padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            height: 50,
                            width: 100,
                            child: FilledButton(
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Holder()));
                                while (widget.queue.isEmpty() == false){
                                  widget.queue.next();
                                }

                              },
                              child: Text(
                                "Skip",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Color(0xFF0560FA),
                                    fontWeight: FontWeight.w700,
                                    height: 10/14
                                ),
                              ),
                              style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll<Color>(Colors.white),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(4.69),
                                          side: BorderSide(width: 1, color: Color(0xFF0560FA))
                                      )
                                  )
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 100,
                            child: FilledButton(
                              onPressed: (){
                                controller.nextPage(duration: Duration(milliseconds: 500), curve: Curves.decelerate);
                              },
                              child: Text(
                                "Next",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  height: 10/14
                                ),
                              ),
                              style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll<Color>(Color(0xFF0560FA)),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(4.69),
                                          side: BorderSide(width: 0, color: Color(0xFF0560FA))
                                      )
                                  )
                              ),
                            ),
                          ),

                        ],
                      ),),
                  SizedBox(height: (widget.queue.isEmpty()) ? 64 : 99)

                ],
              );
            }
        ),
      ),
    );
  }
}
