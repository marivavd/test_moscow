import 'package:flutter/material.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Holder> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('Holder'),
    );
  }
}
